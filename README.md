# Xv6 Operating system
This repository contains modified xv6 source code and is part of the xv6 enhancement project.  
For details of the enhancement project see [here](https://coding.net/u/lqf96/p/xv6-extra/git/blob/master/README.md).  
For original README see [here](README).

## Build
To build this xv6 clone you will need to clone [xv6-extra](https://coding.net/u/lqf96/p/xv6-extra/git) first.  
Open [Makefile](Makefile) and set variable `XV6_EXTRA_DIR` to the location of the xv6-extra repository.
Then build the project with `make`, run xv6 with `make qemu`, or debug the operating system with `make qemu-gdb`.

## License
All code in this repository are licensed under [MIT License](LICENSE).  
Note that most of the enhancement project code are in [xv6-extra](https://coding.net/u/lqf96/p/xv6-extra/git), which is licensed under GNU GPLv3.
